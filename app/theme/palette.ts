export const palette = {
  black: "#1d1d1d",
  white: "#ffffff",
  offWhite: "#e6e6e6",
  orange: "#FBA928",
  orangeDarker: "#EB9918",
  buttonColor: "#75C06E",
  lightGrey: "#939AA4",
  lightGrey90: "#7A7A7A",
  lightGrey100: "#8A8A8A",
  lightGrey200: "#F9F9F9",
  lightGrey50: "#DADADA",
  lighterGrey: "#CDD4DA",
  transparent: "rgba(240,248,255, 0)",
  angry: "#dd3333",
}
