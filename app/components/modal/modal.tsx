/* eslint-disable react-native/no-inline-styles */
import * as React from "react"
import {
  TextStyle,
  View,
  ViewStyle,
  Image,
  ImageSourcePropType,
  Dimensions,
  ImageStyle,
} from "react-native"
import { observer } from "mobx-react-lite"
import { color, typography } from "../../theme"
import { Modal as ModalComponent, Paragraph, Title, Colors } from "react-native-paper"

const windowHeight = Dimensions.get("window").height
const CONTAINER: ViewStyle = { margin: 10, backgroundColor: Colors.white, borderRadius: 20 }

const TEXT: TextStyle = {
  fontFamily: typography.primary,
  fontSize: 14,
  color: color.primary,
}

const IMAGE_STYLE: ImageStyle = {
  height: windowHeight / 3,
  resizeMode: "stretch",
}

export interface ModalProps {
  /**
   * An optional style override useful for padding & margin.
   */
  style?: ViewStyle
  source: ImageSourcePropType
}

/**
 * Describe your component here
 */
export const Modal = observer(function Modal(props: ModalProps) {
  const [modalWidth, setModalWidth] = React.useState(0)

  const [visible, setVisible] = React.useState(false)

  const showModal = () => setVisible(true)
  const hideModal = () => setVisible(false)

  return (
    <ModalComponent visible={visible} onDismiss={hideModal} contentContainerStyle={CONTAINER}>
      <View
        style={{ margin: 25 }}
        onLayout={(event) => setModalWidth(event.nativeEvent.layout.width)}
      >
        <View>
          <Title>Tips for thermometer</Title>
        </View>
        <View style={{ marginVertical: 25 }}>
          <Paragraph>
            Ensure tip of thermometer is embedded in the most center of the protein.
          </Paragraph>
        </View>
        <View style={{ justifyContent: "center" }}>
          <Image style={[{ width: modalWidth }, IMAGE_STYLE]} source={props.source} />
        </View>
      </View>
    </ModalComponent>
  )
})
