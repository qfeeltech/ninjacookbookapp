/* eslint-disable react-native/no-inline-styles */
import React from "react"
import { View } from "react-native"
import { observer } from "mobx-react-lite"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { Screen } from "../../components"
import { BottomNavigation, Text } from "react-native-paper"
import { HomeScreen } from "../../screens"

const MarkRoute = () => {
  return (
    <Screen style={{ flex: 1 }} preset="scroll">
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Text>mark</Text>
      </View>
    </Screen>
  )
}
const DemoRoute = () => {
  return (
    <Screen style={{ flex: 1 }} preset="scroll">
      <View style={{ alignItems: "center", justifyContent: "center" }}>
        <Text>Demo</Text>
      </View>
    </Screen>
  )
}

export const BottomTabsScreen = observer(function BottomTabsScreen() {
  const [index, setIndex] = React.useState(0)

  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  const [routes] = React.useState([
    { key: "home", title: "Home", icon: "home-assistant" },
    { key: "user", title: "User", icon: "account-outline" },
    { key: "mark", title: "Mark", icon: "bookmark-outline" },
    { key: "search", title: "Search", icon: "feature-search-outline" },
  ])

  const renderScene = BottomNavigation.SceneMap({
    home: HomeScreen,
    user: DemoRoute,
    mark: MarkRoute,
    search: DemoRoute,
  })

  return (
    <BottomNavigation
      navigationState={{ index, routes }}
      onIndexChange={(index) => setIndex(index)}
      renderScene={renderScene}
    />
  )
})
