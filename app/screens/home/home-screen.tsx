/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import {
  TextStyle,
  ViewStyle,
  View,
  Text,
  Dimensions,
  ScrollView,
  Image,
  TouchableOpacity,
} from "react-native"
import { Screen } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { IconButton, Caption, Colors, Appbar } from "react-native-paper"
import CardView from "react-native-cardview"
import Carousel from "react-native-snap-carousel"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const ROOT: ViewStyle = {
  backgroundColor: color.palette.lightGrey200,
  flex: 1,
}

const BROWSE_CONTAINER: ViewStyle = {
  backgroundColor: color.palette.white,
}

const TITLE_STYLE: ViewStyle = {
  justifyContent: "center",
  alignItems: "center",
}

const KEYS_STYLE: ViewStyle = { marginBottom: 30 }
const CAROUSEL_CONTAINER: ViewStyle = {
  flexDirection: "row",
  justifyContent: "center",
  width: windowWidth,
  height: windowHeight * 0.25,
}
const EXPLORER_CONTAINER: ViewStyle = {}
const TEXT_LABEL_STYLE: TextStyle = {
  fontFamily: "Gotham-Book",
  fontSize: 16,
  color: color.palette.lightGrey90,
}
const KEY_LABEL_STYLE: TextStyle = {
  fontSize: 16,
  fontFamily: "Gotham-Book",
  color: color.palette.lightGrey100,
}

const KEY_CONTAINER: ViewStyle = {
  // borderWidth: 1,
  borderColor: color.palette.lightGrey50,
  width: (windowWidth - 30) * 0.3,
  height: 45,
  alignItems: "center",
  justifyContent: "center",
}

const KEYS: string[] = [
  "Sirloin",
  "Filet",
  "Flank",
  "NY Strip",
  "Ribeye",
  "Brisket",
  "Flat Iron",
  "T-Bone",
  "Rib",
]

const groupArray = (data, cols) => {
  return data
    .reduce(
      ([groups, subIndex], d) => {
        if (subIndex === 0) {
          groups.unshift([])
        }
        groups[0].push(d)
        return [groups, (subIndex + 1) % cols]
      },
      [[], 0],
    )[0]
    .reverse()
}

export const HomeScreen = observer(function HomeScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  const [foodKeys, setFoodKeys] = React.useState([
    "Sirloin",
    "Filet",
    "Flank",
    "NY Strip",
    "Ribeye",
    "Brisket",
    "Flat Iron",
    "T-Bone",
    "Rib",
  ])

  const [recipes, setRecipes] = React.useState([
    {
      title: "Garlic butter baked salmon with Greek salad",
      uri: "https://picsum.photos/200",
      img: require("./beef.png"),
      cookTime: "30 minutes",
    },
    {
      title: "Garlic butter baked salmon with Greek salad",
      uri: "https://picsum.photos/200",
      img: require("./Spice-crusted-Lamb-with-White-Bean-Tomato-Salad.png"),
      cookTime: "30 minutes",
    },
    {
      title: "Garlic butter baked salmon with Greek salad",
      uri: "https://picsum.photos/200",
      img: require("./unnamed.png"),
      cookTime: "30 minutes",
    },
  ])

  const [carouselItems] = React.useState([
    {
      img: require("./chicken_2in.png"),
      keys: [],
    },
    {
      img: require("./Beef_2in.png"),
      keys: [
        "Sirloin",
        "Filet",
        "Flank",
        "NY Strip",
        "Ribeye",
        "Brisket",
        "Flat Iron",
        "T-Bone",
        "Rib",
      ],
    },
    {
      img: require("./fish_2in.png"),
      keys: [],
    },
  ])

  const [firstItem, setFirstItem] = React.useState(1)

  useEffect(() => {
    // setTimeout(() => {
    //   setFirstItem(1)
    // }, 500)
  }, [])

  // Pull in navigation via hook
  const navigation = useNavigation()

  const renderItem = ({ item }) => {
    return (
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
        }}
      >
        <Image style={{ width: windowWidth * 0.5, height: windowWidth * 0.5 }} source={item.img} />
      </View>
    )
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Browse Ingredients" />
      </Appbar.Header>
      <ScrollView>
        <View style={BROWSE_CONTAINER}>
          <View style={TITLE_STYLE}>
            <Text
              style={{
                fontFamily: "Gotham-Book",
                fontWeight: "bold",
                fontSize: 16,
                marginVertical: 15,
              }}
            >
              Beef
            </Text>
          </View>
          <View style={CAROUSEL_CONTAINER}>
            <Carousel
              data={carouselItems}
              sliderWidth={windowWidth}
              itemWidth={windowWidth * 0.5}
              renderItem={renderItem}
              slideStyle={{
                marginHorizontal: 5,
                alignItems: "center",
                justifyContent: "center",
              }}
              firstItem={firstItem}
              onSnapToItem={(slideIndex) => console.log(`slideIndex: ${slideIndex}`)}
            />
          </View>
          {/* keys */}
          <View style={KEYS_STYLE}>
            {groupArray(KEYS, 3).map((keys, index) => {
              return (
                <View
                  key={index}
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                    justifyContent: "center",
                  }}
                >
                  {keys.map((value, index0) => {
                    return (
                      <TouchableOpacity
                        key={index0}
                        onPress={() => navigation.navigate("cookSettings")}
                      >
                        <View
                          style={[
                            KEY_CONTAINER,
                            index0 === 1 ? { borderRightWidth: 1, borderLeftWidth: 1 } : {},
                            index !== 0 ? { borderTopWidth: 1 } : {},
                          ]}
                        >
                          <Text style={KEY_LABEL_STYLE}>{value}</Text>
                        </View>
                      </TouchableOpacity>
                    )
                  })}
                </View>
              )
            })}
          </View>
        </View>
        <View style={EXPLORER_CONTAINER}>
          <View style={{ alignItems: "center", justifyContent: "center", margin: 15 }}>
            <Text style={TEXT_LABEL_STYLE}>Explore Recipes</Text>
          </View>
          <View style={{ alignItems: "center" }}>
            {recipes.map((item, index) => {
              return (
                <CardView
                  key={index}
                  cardElevation={2}
                  cardMaxElevation={2}
                  cornerRadius={20}
                  style={{
                    width: windowWidth * 0.9,
                    height: windowHeight * 0.15,
                    backgroundColor: Colors.white,
                    margin: 10,
                    justifyContent: "center",
                  }}
                >
                  <View style={{ margin: 10, flexDirection: "row" }}>
                    <Image
                      style={{
                        width: windowWidth * 0.25,
                        height: windowWidth * 0.25,
                        borderRadius: 5,
                      }}
                      source={item.img}
                    />
                    <View style={{ width: windowWidth * 0.45, marginLeft: 10 }}>
                      <Text style={{ fontFamily: "Gotham-Book", fontSize: 16 }}>{item.title}</Text>
                      <Caption style={{ marginVertical: 10 }}>Cook time: {item.cookTime}</Caption>
                    </View>
                    <View style={{ margin: -16 }}>
                      <IconButton
                        icon="bookmark-outline"
                        color={Colors.grey500}
                        size={30}
                        onPress={() => console.log("Pressed")}
                      />
                    </View>
                  </View>
                </CardView>
              )
            })}
          </View>
        </View>
      </ScrollView>
    </Screen>
  )
})
