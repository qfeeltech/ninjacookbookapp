/* eslint-disable react-native/no-inline-styles */
import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, ImageBackground, View, Image, Dimensions } from "react-native"
import { Screen } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { Button, Colors } from "react-native-paper"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const BUTTON_STYLE: ViewStyle = {
  marginBottom: 20,
  borderRadius: 100,
}

const BUTTON_CONTENT_STYLE: ViewStyle = {
  width: windowWidth * 0.8,
  height: 50,
}

export const SignInScreen = observer(function SignInScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  return (
    <Screen style={ROOT} unsafe={true}>
      <ImageBackground style={{ flex: 1 }} source={require("./bg.png")}>
        <View
          style={{
            alignItems: "flex-start",
            justifyContent: "flex-start",
            marginTop: 40,
            marginLeft: -((windowHeight * 0.3) / 2.5),
          }}
        >
          <Image
            style={{ width: windowHeight * 0.25, height: windowHeight * 0.25 }}
            source={require("./sighin_beefdinner1.png")}
          />
        </View>
        <View style={{ alignItems: "center" }}>
          <View style={{ marginBottom: 25, alignItems: "center" }}>
            <Image
              style={{ width: windowWidth * 0.5, height: windowWidth * 0.3 }}
              source={require("./logo.png")}
            />
          </View>
          <Button
            mode="contained"
            style={BUTTON_STYLE}
            color={color.palette.buttonColor}
            uppercase={false}
            contentStyle={BUTTON_CONTENT_STYLE}
            labelStyle={{ color: Colors.white, fontFamily: "Gotham-Book" }}
            onPress={() => navigation.navigate("setup")}
          >
            New Device
          </Button>
          <Button
            mode="contained"
            style={BUTTON_STYLE}
            contentStyle={BUTTON_CONTENT_STYLE}
            color={color.palette.buttonColor}
            uppercase={false}
            labelStyle={{ color: Colors.white, fontFamily: "Gotham-Book" }}
            onPress={() => console.log("Create Account")}
          >
            Create Account
          </Button>
          <Button
            mode="contained"
            style={[BUTTON_STYLE, { borderColor: color.palette.buttonColor, borderWidth: 1 }]}
            contentStyle={BUTTON_CONTENT_STYLE}
            uppercase={false}
            labelStyle={{ color: Colors.black, fontFamily: "Gotham-Book" }}
            onPress={() => navigation.navigate("bottomTabs")}
          >
            Explore
          </Button>
        </View>
        <View
          style={{
            alignItems: "flex-end",
            justifyContent: "flex-end",
            marginRight: -((windowHeight * 0.3) / 2.5),
          }}
        >
          <Image
            style={{ width: windowHeight * 0.25, height: windowHeight * 0.25 }}
            source={require("./sighin_beefdinner2.png")}
          />
        </View>
      </ImageBackground>
    </Screen>
  )
})
