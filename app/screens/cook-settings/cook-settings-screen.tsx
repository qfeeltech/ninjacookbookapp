/* eslint-disable react-native/no-inline-styles */
import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, TextStyle, View, Text, Dimensions, Image, ScrollView } from "react-native"
import { Screen } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { Appbar, Colors, IconButton, Button } from "react-native-paper"
import { Slider } from "react-native-elements"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}
const TEXT_LABEL: TextStyle = { fontFamily: "Gotham-Book", fontSize: 16 }
const TEXT_LABEL_BOLD: TextStyle = { fontFamily: "Gotham-Book", fontSize: 16, fontWeight: "bold" }
const TEXT_STYLE: TextStyle = { fontSize: 16 }
const BUTTON_CONTENT_STYLE: ViewStyle = {
  width: windowWidth * 0.65,
  height: 50,
}

export const CookSettingsScreen = observer(function CookSettingsScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  return (
    <Screen style={ROOT} preset="scroll">
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content
          title={
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Text
                style={{
                  fontSize: 16,
                  fontFamily: "Gotham-Book",
                  textAlign: "center",
                }}
              >
                Cook Settings
              </Text>
            </View>
          }
          style={{ flex: 1 }}
        />
      </Appbar.Header>
      <ScrollView style={{ backgroundColor: color.palette.lightGrey200 }}>
        <View style={{ flex: 1, backgroundColor: Colors.white }}>
          <View style={{ marginHorizontal: 35, marginTop: 5 }}>
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Text style={{ fontFamily: "Gotham-Book", fontSize: 21, fontWeight: "bold" }}>
                Ribeye
              </Text>
            </View>
            <View style={{ alignItems: "center", justifyContent: "center", marginVertical: 15 }}>
              <Image
                style={{ width: windowWidth * 0.5, height: windowWidth * 0.5 }}
                source={require("./Beef_2in.png")}
              />
            </View>
            <View style={{ marginVertical: 10 }}>
              <Text style={TEXT_LABEL}>Cook Time: 10 minutes</Text>
            </View>
            <View style={{ marginVertical: 10 }}>
              <Text style={TEXT_LABEL_BOLD}>Doneness</Text>
            </View>
            <View style={{ alignItems: "stretch", justifyContent: "center", marginVertical: 10 }}>
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  marginBottom: -10,
                }}
              >
                <Text style={[TEXT_LABEL, { color: color.palette.lightGrey90 }]}>Rare</Text>
                <Text style={[TEXT_LABEL, { color: color.palette.lightGrey90 }]}>Medium</Text>
                <Text style={[TEXT_LABEL, { color: color.palette.lightGrey90 }]}>Well</Text>
              </View>
              <Slider
                maximumValue={9}
                minimumValue={1}
                step={1}
                maximumTrackTintColor={color.palette.lightGrey50}
                minimumTrackTintColor={color.palette.transparent}
                trackStyle={{
                  height: 8,
                  borderRadius: 20,
                }}
                thumbStyle={{ height: 15, width: 15, backgroundColor: Colors.grey600 }}
              />
              <View
                style={{ flexDirection: "row", justifyContent: "space-between", marginTop: -8 }}
              >
                {numbers.map((item, index) => (
                  <Text style={{ fontSize: 10, color: color.palette.lightGrey100 }} key={index}>
                    {item}
                  </Text>
                ))}
              </View>
            </View>
            <View
              style={{
                flexDirection: "row",
                alignItems: "center",
                justifyContent: "space-between",
                marginVertical: 10,
              }}
            >
              <Text style={TEXT_LABEL_BOLD}>Thickness</Text>
              <Text style={TEXT_LABEL}>1 in</Text>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                }}
              >
                <IconButton
                  icon="minus"
                  color={Colors.white}
                  style={{ backgroundColor: Colors.grey600 }}
                  size={25}
                  onPress={() => console.log("Pressed")}
                />
                <IconButton
                  icon="plus"
                  color={Colors.white}
                  style={{ backgroundColor: Colors.grey600 }}
                  size={25}
                  onPress={() => console.log("Pressed")}
                />
              </View>
            </View>
            <View style={{ marginVertical: 15, alignItems: "center", marginBottom: 30 }}>
              <Button
                uppercase={false}
                mode="contained"
                style={{ borderRadius: 100 }}
                labelStyle={{ color: Colors.white, fontFamily: "Gotham-Book", fontSize: 19 }}
                contentStyle={BUTTON_CONTENT_STYLE}
                color={color.palette.buttonColor}
                onPress={() => {
                  console.log("Start cooking")
                  navigation.navigate("cook")
                }}
              >
                Start cooking!
              </Button>
            </View>
          </View>
        </View>
        <View>
          <View style={{ alignItems: "center", justifyContent: "center", marginVertical: 15 }}>
            <Text style={TEXT_LABEL}>Explore Recipes</Text>
          </View>
        </View>
      </ScrollView>
    </Screen>
  )
})
