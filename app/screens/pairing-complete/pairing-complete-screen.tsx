/* eslint-disable react-native/no-inline-styles */
import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, View, Dimensions, Image, Text } from "react-native"
import { Screen } from "../../components"
import { useNavigation, StackActions } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { Caption } from "react-native-paper"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

export const PairingCompleteScreen = observer(function PairingCompleteScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  React.useEffect(() => {
    setTimeout(() => {
      const { index, routes } = navigation.dangerouslyGetState()
      const currentRoute = routes[index].name
      if (currentRoute === "paringComplete") {
        navigation.dispatch(StackActions.popToTop())
      }
    }, 1000 * 5)
  }, [])

  return (
    <Screen style={ROOT} preset="fixed">
      <View
        style={{
          flexDirection: "row",
        }}
      >
        <View
          style={{
            marginLeft: -((windowHeight * 0.3) / 2),
            marginTop: windowHeight * 0.1,
          }}
        >
          <Image
            style={{
              width: windowHeight * 0.3,
              height: windowHeight * 0.3,
            }}
            source={require("./Pairing_compete2.png")}
          />
        </View>
        <View
          style={{
            marginLeft: (windowHeight * 0.3) / 2,
          }}
        >
          <Image
            style={{
              width: windowHeight * 0.3,
              height: windowHeight * 0.3,
            }}
            source={require("./Pairing_compete4.png")}
          />
        </View>
      </View>
      <View
        style={{
          alignItems: "center",
          justifyContent: "center",
          marginVertical: 20,
        }}
      >
        <Text style={{ fontSize: 20, fontFamily: "Gotham-Bold", marginBottom: 20 }}>
          Pairing complete
        </Text>
        <Caption
          style={{
            fontSize: 16,
            textAlign: "center",
            width: windowWidth * 0.9,
          }}
        >
          Your Foodi Smart Indoor Grill is now connected.
        </Caption>
      </View>
      <View
        style={{
          flexDirection: "row",
        }}
      >
        <View
          style={{
            marginLeft: -((windowHeight * 0.3) / 2),
          }}
        >
          <Image
            style={{
              width: windowHeight * 0.3,
              height: windowHeight * 0.3,
            }}
            source={require("./Pairing_compete1.png")}
          />
        </View>
        <View
          style={{
            marginTop: windowHeight * 0.1,
            marginLeft: (windowHeight * 0.3) / 2,
          }}
        >
          <Image
            style={{
              width: windowHeight * 0.3,
              height: windowHeight * 0.3,
            }}
            source={require("./Pairing_compete3.png")}
          />
        </View>
      </View>
    </Screen>
  )
})
