/* eslint-disable react-native/no-inline-styles */
import React from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, Image, View, Dimensions } from "react-native"
import { Screen } from "../../components"
import { useNavigation, StackActions } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { Appbar, Title, Caption, Button, Colors } from "react-native-paper"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

const BUTTON_STYLE: ViewStyle = {
  marginBottom: 20,
  borderRadius: 100,
}

const BUTTON_CONTENT_STYLE: ViewStyle = {
  width: windowWidth * 0.8,
  height: 50,
}

export const PairingScreen = observer(function PairingScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  const _goHome = () => {
    console.log("go home screen")
    navigation.navigate("signin")
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content title="Pairing" titleStyle={{ fontSize: 18, fontWeight: "bold" }} />
      </Appbar.Header>
      <View>
        <Image
          style={{ width: windowWidth, height: windowHeight * 0.4, resizeMode: "center" }}
          source={require("./grill.png")}
        />
      </View>
      <View style={{ flex: 1, alignItems: "center", margin: 35 }}>
        <Title>Foodi Smart Indoor Grill detected</Title>
        <View>
          <Caption style={{ fontSize: 16, marginVertical: 10 }}>
            Is this the product to pair?
          </Caption>
        </View>
        <View style={{ marginVertical: 20 }}>
          <Button
            mode="contained"
            style={BUTTON_STYLE}
            contentStyle={BUTTON_CONTENT_STYLE}
            color={color.palette.buttonColor}
            uppercase={false}
            labelStyle={{ color: Colors.white, fontFamily: "Gotham-Book", fontSize: 16 }}
            onPress={() => navigation.navigate("paringComplete")}
          >
            Yes, Pair!
          </Button>
          <Button
            mode="contained"
            style={[BUTTON_STYLE, { borderColor: color.palette.buttonColor, borderWidth: 1 }]}
            contentStyle={BUTTON_CONTENT_STYLE}
            uppercase={false}
            labelStyle={{ color: Colors.green500, fontFamily: "Gotham-Book", fontSize: 16 }}
            onPress={() => navigation.dispatch(StackActions.popToTop())}
          >
            Cancel
          </Button>
        </View>
      </View>
    </Screen>
  )
})
