/* eslint-disable react-native/no-inline-styles */
import React, { useState, useEffect, useRef } from "react"
import { observer } from "mobx-react-lite"
import {
  ViewStyle,
  ScrollView,
  Text,
  View,
  ImageStyle,
  TextStyle,
  Dimensions,
  Image,
  DeviceEventEmitter,
  ImageBackground,
  findNodeHandle,
} from "react-native"
import { Screen, Modal } from "../../components"
import { useNavigation, StackActions } from "@react-navigation/native"
// import { useStores } from "../../models"
import { Appbar, Button, Card, Paragraph, Colors, IconButton } from "react-native-paper"
import * as Progress from "react-native-progress"
import { Slider } from "react-native-elements"
import { color } from "../../theme"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height
const buttonColor = "#75C06E"
const PROGRESS_BAR_COLOR = color.palette.lightGrey100

const ROOT: ViewStyle = {
  backgroundColor: "#F8F8F8",
  flex: 1,
}

const STEAK_IMG = require("./steak0.png")

interface CookingStepsProps {
  step: number
  summary: {
    title: string
    desc: string[]
  }[]
  source?: any
  helper?: {
    title: string
    source: any
  }
  progressBar: {
    type: number
    disabled?: boolean
    prompt: string
    showButton: boolean
    endValue?: number
    value: number
    messages?: { label: string; val: any; unit: string }[]
  }[]
}

interface CookSettingsProps {
  cookTime: number
  doneness: number
  thickness: number
  source: any
  ingredients: string[]
}

const COOKBOOK_SUMMARY: CookSettingsProps = {
  cookTime: 40,
  doneness: 5,
  thickness: 1,
  source: require("./steak0.png"),
  ingredients: [
    "Ribeye steak",
    "1/2 tablespoon canola oil",
    "Cauliflower",
    "1/3 cup olives",
    "1/2 cup roasted red peppers",
    "1 tbsp minced oregano",
    "1 tbsp minced parsley",
    "3 minced garlic cloves",
    "1 lemon (juice)",
    "1lb crumbled feta",
    "Salt and pepper",
  ],
}

const COOKING_STEPS: CookingStepsProps[] = [
  {
    step: 1,
    summary: [
      {
        title: "1. Prep steak",
        desc: [
          "Brush each steak on all sides with canola oil.",
          "Season with salt and pepper as desired.",
        ],
      },
      {
        title: "You'll need:",
        desc: ["Ribeye steak", "1/2 tablespoon canola oil salt and pepper"],
      },
    ],
    source: require("./steak1.png"),
    progressBar: [
      {
        type: 0, // control
        disabled: true,
        prompt: "Steak is prepared",
        showButton: true,
        value: 0,
        endValue: 8,
      },
    ],
  },
  {
    step: 2,
    summary: [
      {
        title: "2. Preheat unit",
        desc: ["Press start/stop on unit to start preheating."],
      },
    ],
    progressBar: [
      {
        type: 0, // control
        disabled: true,
        prompt: "Ready to Preheat",
        showButton: false,
        value: 0,
        endValue: 8,
      },
    ],
  },
  {
    step: 3,
    summary: [
      {
        title: "3. Prep cauliflower and salsa",
        desc: [
          `Cut cauliflower from top to bottom into two 2-inch "steaks"; reserve remaining cauliflower.`,
          `Season with salt and pepper as desired.`,
          `To make the Greek salsa, in a large bowl, stir together olives, roasted red peppers, oregano, parsley, garlic, lemon juice, feta, salt, pepper, walnuts, red onion, and 2 tablespoons of canola oil.`,
          `Set Aside.`,
        ],
      },
      {
        title: "You'll need:",
        desc: [
          `Cauliflower`,
          `1/3 cup olives`,
          `1/2 cup roasted red peppers`,
          `1 tbsp minced oregano`,
          `1 tbsp minced parsley`,
          `3 minced garlic cloves`,
          `1 lemon (juice)`,
          `1lb crumbled feta`,
          `Salt and pepper`,
        ],
      },
    ],
    source: require("./steak_3.png"),
    progressBar: [
      {
        type: 0, // control
        disabled: true,
        prompt: "Preheating complete",
        showButton: true,
        value: 0,
        endValue: 8,
      },
    ],
  },
  {
    step: 4,
    summary: [
      {
        title: "4. Add steak",
        desc: [
          `Set up thermometer with steak and place both in grill.`,
          `Place steak on grill plate.`,
          `Close lid.`,
        ],
      },
      {
        title: "You'll need:",
        desc: [`Ribeye steak`, `Thermometer`],
      },
    ],
    helper: {
      title: "Tips for thermometer",
      source: require("./steak_4_1.png"),
    },
    source: require("./steak_4.png"),
    progressBar: [
      {
        type: 0, // control
        disabled: true,
        prompt: "Steak is added",
        showButton: true,
        value: 0,
        endValue: 8,
      },
    ],
  },
  {
    step: 5,
    summary: [
      {
        title: "5. Flip steak",
        desc: [`Use cooking tongs to flip steak.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cooking tongs`],
      },
    ],
    source: require("./steak_5.png"),
    progressBar: [
      {
        type: 1, // [0]current, [1]target
        prompt: "",
        messages: [
          { label: "Current temp", val: 105, unit: "F" },
          { label: "Target temp", val: 145, unit: "F" },
        ],
        value: 0,
        showButton: false,
      },
    ],
  },
  {
    step: 6,
    summary: [
      {
        title: "6. Remove steak to rest",
        desc: [
          "Use cooking tongs to remove and transfer steak to a plate or cutting board.",
          "Leave the thermometer in the steak to track resting progress and keep the steak juicy & tender.",
        ],
      },
      {
        title: "You'll need:",
        desc: ["Cooking tongs", "Plate or cutting board"],
      },
    ],
    source: require("./steak_6.png"),
    progressBar: [
      {
        type: 0, // control
        disabled: true,
        prompt: "Steak is removed",
        showButton: true,
        value: 0,
        endValue: 8,
      },
    ],
  },
  {
    step: 7,
    summary: [
      {
        title: "7. Add cauliflower",
        desc: [`Use cooking tongs to place seasoned cauliflower steaks on grill.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cauliflower steaks`, `Cooking tongs`],
      },
    ],
    source: require("./steak_7.png"),
    progressBar: [
      {
        type: 0, // control
        disabled: true,
        prompt: "Continue cooking",
        showButton: true,
        value: 0,
        endValue: 8,
      },
      {
        type: 2, // countdown timer
        prompt: "",
        messages: [{ label: "Steak resting", val: "1:15", unit: "" }],
        value: 0,
        showButton: true,
      },
    ],
  },
  {
    step: 8,
    summary: [
      {
        title: "8. Flip cauliflower",
        desc: [`Use cooking tongs to flip cauliflower steak.`, `Close lid.`],
      },
      {
        title: "You'll need:",
        desc: [`Cooking tongs`],
      },
    ],
    source: require("./steak_8.png"),
    progressBar: [
      {
        type: 2, // countdown timer
        prompt: "",
        messages: [{ label: "Countdown time", val: "4:59", unit: "" }],
        showButton: true,
        value: 0,
      },
      {
        type: 2, // countdown timer
        prompt: "",
        messages: [{ label: "Steak resting", val: "4:59", unit: "" }],
        value: 0,
        showButton: true,
      },
    ],
  },
  {
    step: 9,
    summary: [
      {
        title: `9. Coat cauliflower with salsa`,
        desc: [
          `While cauliflower steaks are on the grill, use spoon to generously coat with salsa.`,
          `Close lid.`,
        ],
      },
      {
        title: "You'll need:",
        desc: [`Cauliflower steaks`, `Cooking tongs`],
      },
    ],
    source: require("./steak_9.png"),
    progressBar: [
      {
        type: 2, // countdown timer
        prompt: "",
        messages: [{ label: "Countdown time", val: "1:59", unit: "" }],
        value: 0,
        showButton: true,
      },
      {
        type: 2, // countdown timer
        prompt: "",
        messages: [{ label: "Steak resting", val: "1:59", unit: "" }],
        value: 0,
        showButton: true,
      },
    ],
  },
]

const CARD_CONTAINER: ViewStyle = { borderRadius: 20 }
const CARD_COVER_CONTAINER: ImageStyle = {
  height: windowHeight / 3,
  borderTopLeftRadius: 20,
  borderTopRightRadius: 20,
}
const ITEM_CONTAINER: ViewStyle = { marginHorizontal: 5, padding: 10 }
const CONTROL_CONTAINER: ViewStyle = { marginVertical: 15, alignItems: "center" }
const PROGRESS_BAR_CONTAINER: ViewStyle = { alignItems: "center" }
const PROGRESS_BAR: ViewStyle = {
  borderRadius: 20,
  height: 10,
  width: windowWidth / 1.3,
  backgroundColor: color.palette.white,
}
const CONTROL: ViewStyle = {
  flexDirection: "row",
  justifyContent: "space-between",
  alignItems: "center",
  width: windowWidth / 1.3,
  marginBottom: 5,
}
const NEXT_BUTTON: ViewStyle = { borderRadius: 20, width: 100 }
const TEXT_LABEL: TextStyle = { fontSize: 16, fontFamily: "Gotham-Book", color: "#7A7A7A" }
const TEXT_LABEL_BOLD: TextStyle = { fontSize: 16, fontFamily: "Gotham-Book", fontWeight: "bold" }
const BUTTON_CONTENT_STYLE: ViewStyle = {
  width: windowWidth * 0.65,
  height: 50,
}

const GRAYSCALE_STYLE: ViewStyle = { opacity: 0.3 }
const END_VALUE = 1
const numbers = [1, 2, 3, 4, 5, 6, 7, 8, 9]

const COOKING_STEPS_CHANNEL = "cookingSteps"

const timeToSecond = (time) => {
  const [min, sec] = time.split(":")
  return Number(min * 60) + Number(sec)
}

const secondToTime = (s: number) => {
  let t = ""
  if (s > -1) {
    const min = Math.floor(s / 60) % 60
    const sec = s % 60
    if (min < 10) {
      t += "0"
    }
    t += min + ":"
    if (sec < 10) {
      t += "0"
    }
    t += sec.toFixed(0)
  }
  return t
}

export const CookScreen = observer(function CookScreen() {
  const itemRefs = React.useRef([])
  const scrollViewRef = React.useRef(undefined)

  const [visible, setVisible] = React.useState(false)
  const showModal = () => setVisible(true)
  const hideModal = () => setVisible(false)

  const [cookSettings, setCookSettings] = useState(COOKBOOK_SUMMARY)
  const [cookingSteps, setCookingSteps] = useState(COOKING_STEPS)
  const currentStepRef = useRef(0)
  const [startStatus, setStartStatus] = useState(false)
  const [enjoyStatus, setEnjoyStatus] = useState(true)

  useEffect(() => {
    // {index: {timeout: id, countdown: seconds}}
    const interval = {}
    const countdownTimerSubs = []
    for (let i = 0; i < cookingSteps.length; i++) {
      const newItem = cookingSteps[i]
      for (let j = 0; j < newItem.progressBar.length; j++) {
        const progressBarItem = newItem.progressBar[j]
        if (progressBarItem.type === 2) {
          const channel = `countdownTimer_${newItem.step - 1}_${j}`
          console.log(channel)
          const subscription = DeviceEventEmitter.once(
            channel,
            (data) => {
              console.log(data)
              const index = data.index
              const level = data.level
              const id = setInterval(() => {
                setCookingSteps((oldItems) => {
                  const newItems = [...oldItems]
                  for (let i = 0; i < newItems[index].progressBar.length; i++) {
                    const progressBarItem = newItems[index].progressBar[i]
                    // console.log(i, progressBarItem)
                    const intervalItem = interval[`${index}_${level}`]
                    console.log(interval)
                    if (progressBarItem.type === 2) {
                      for (let j = 0; j < progressBarItem.messages.length; j++) {
                        const item = progressBarItem.messages[j]
                        let seconds = timeToSecond(item.val)
                        let countdown
                        if (Object.keys(intervalItem).indexOf("countdown") !== -1) {
                          countdown = intervalItem.countdown
                        } else {
                          intervalItem.countdown = seconds
                          countdown = seconds
                        }
                        seconds -= 1
                        if (seconds <= 0) clearInterval(intervalItem.timer)
                        const time = secondToTime(seconds)
                        item.val = time
                        progressBarItem.value = (countdown - seconds) / countdown
                        console.log(`time: ${time}, value: ${progressBarItem.value}`)
                        if (progressBarItem.value === 1) {
                          console.log(`time finish.`)
                          const itemIndex = intervalItem.index
                          if (itemIndex) {
                            const progressBar0 = newItems[index].progressBar[itemIndex]
                            console.log(progressBar0)
                            if (progressBar0.value === END_VALUE) {
                              newItems[index].progressBar[itemIndex].disabled = false
                            }
                          } else {
                            onNextStep(newItems[index].step)
                          }
                        }
                      }
                    } else if (progressBarItem.showButton && !intervalItem.index) {
                      console.log(`have Next button`)
                      intervalItem.index = i
                    }
                  }
                  return newItems
                })
              }, 1000 * 1)
              interval[`${index}_${level}`] = { timer: id }
            },
            [],
          )
          countdownTimerSubs.push(subscription)
        }
      }
    }

    const subscription = DeviceEventEmitter.addListener(COOKING_STEPS_CHANNEL, (data) => {
      // {step: 1, progress: [{val: 8}]}
      // console.log(data)
      if (!data || !data.step) return
      const index = data.step - 1
      if (index < 0) return
      console.log(`currentStep: ${currentStepRef.current}, index: ${index}`)
      const newItems = [...cookingSteps]
      // progressBar
      for (let i = 0; i < data.progress.length; i++) {
        const progressItem = data.progress[i]
        const progressBarItem = newItems[index].progressBar[i]
        if (progressBarItem.type === 0) {
          // next control
          progressBarItem.value = progressItem.val / progressBarItem.endValue
          if (progressBarItem.value === END_VALUE && newItems[index].progressBar.length === 1) {
            progressBarItem.disabled = false
            if (!progressBarItem.showButton) {
              onNextStep(data.step)
            }
          }
        } else if (progressBarItem.type === 1) {
          // current, target
          const length = progressBarItem.messages.length
          if (length !== 2) return
          progressBarItem.messages[0].val = progressItem.val
          progressBarItem.value = progressBarItem.messages[0].val / progressBarItem.messages[1].val
          if (progressBarItem.value === END_VALUE) {
            if (!progressBarItem.showButton) {
              onNextStep(data.step)
            }
          }
        } else if (progressBarItem.type === 2) {
          // timer
          console.log(`countdownTimer_${index}_${i}`)
          DeviceEventEmitter.emit(`countdownTimer_${index}_${i}`, { index: index, level: i })
        }
      }
      for (let i = 0; i < index; i++) {
        const progressBarItem = newItems[i].progressBar[0]
        if (progressBarItem.type === 0 && !progressBarItem.disabled) {
          console.log(`next button disabled index: ${i}`)
          progressBarItem.disabled = true
        }
      }
      setCookingSteps(newItems) // Update Cooking Steps
    })

    setTimeout(() => {
      // DeviceEventEmitter.emit("countdownTimer", { index: 6 })
    }, 1000 * 3)

    return () => {
      console.log(`cook component Will UnMount`)
      subscription.remove()

      for (const sub of countdownTimerSubs) {
        sub.remove()
      }

      for (const index of Object.keys(interval)) {
        console.log(index)
        clearInterval(interval[index].timer)
      }
    }
  }, [])

  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  // Pull in navigation via hook
  const navigation = useNavigation()

  const scrollToItem = (index) => {
    console.log(`scroll to item index: ${index}`)
    // scroll to item
    if (index === undefined) return
    const refToScroll = itemRefs.current[index]
    const cookItem = cookingSteps[index]
    currentStepRef.current = cookItem.step
    if (refToScroll === null) return
    refToScroll.measureLayout(findNodeHandle(scrollViewRef.current), (x, y) => {
      scrollViewRef.current.scrollTo({ x: 0, y: y, animated: true })
    })
  }

  const onNextStep = (step: number) => {
    console.log(`onNextStep: ${step}`)
    if (step === 0) {
      setStartStatus(!startStatus)
    } else if (cookingSteps.length === step) {
      console.log(`Enjoy!`)
      setEnjoyStatus(false)
      return
    }

    if ([0, 1, 2, 3, 5].indexOf(step) !== -1) {
      for (let i = 0; i < 8; i++) {
        setTimeout(() => {
          DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
            step: step + 1,
            progress: [{ val: i + 1 }],
          })
        }, 1000 * (1 * (i + 1)))
      }
    } else if ([4].indexOf(step) !== -1) {
      for (let i = 105; i <= 145; i += 5) {
        setTimeout(() => {
          DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
            step: step + 1,
            progress: [{ val: i }],
          })
        }, 1000 * 1 * (i - 100))
      }
    } else if ([6, 7, 8].indexOf(step) !== -1) {
      for (let i = 0; i < 8; i++) {
        setTimeout(() => {
          DeviceEventEmitter.emit(COOKING_STEPS_CHANNEL, {
            step: step + 1,
            progress: [{ val: i + 1 }, {}],
          })
        }, 1000 * (1 * (i + 1)))
      }
    }

    scrollToItem(step)
  }

  const StepItem = ({ item, index }) => {
    return (
      <View key={index} style={[ITEM_CONTAINER, {}]} ref={(ref) => (itemRefs.current[index] = ref)}>
        <Card style={CARD_CONTAINER}>
          {item.source ? (
            <Card.Cover
              style={[
                CARD_COVER_CONTAINER,
                currentStepRef.current >= item.step ? {} : GRAYSCALE_STYLE,
              ]}
              source={item.source}
            />
          ) : null}
          <Card.Content style={currentStepRef.current >= item.step ? {} : GRAYSCALE_STYLE}>
            {item.summary.map((info, index1) => {
              return (
                <View
                  key={index1}
                  style={index1 === 0 ? { marginTop: 30 } : { marginVertical: 20 }}
                >
                  <Text style={TEXT_LABEL_BOLD}>{info.title}</Text>
                  {info.desc.map((_d, index2) => {
                    return (
                      <Paragraph key={index2} style={index1 === 0 ? { marginTop: 10 } : {}}>
                        {_d}
                      </Paragraph>
                    )
                  })}
                </View>
              )
            })}
            {item.helper ? (
              <View
                style={{
                  flexDirection: "row",
                  justifyContent: "space-between",
                  alignItems: "center",
                }}
              >
                <Text style={{ fontFamily: "Gotham-Book", fontSize: 16 }}>{item.helper.title}</Text>
                <IconButton
                  icon="help-circle"
                  color={Colors.green400}
                  size={35}
                  onPress={() => console.log("help")}
                />
              </View>
            ) : null}
          </Card.Content>
        </Card>
        {item.progressBar &&
          item.progressBar.map((progressBar, index) => {
            return (
              <View
                key={index}
                style={[
                  CONTROL_CONTAINER,
                  currentStepRef.current >= item.step ? {} : GRAYSCALE_STYLE,
                ]}
              >
                {progressBar.type === 0 ? (
                  <>
                    <View style={CONTROL}>
                      <Text style={{ fontFamily: "Gotham-Book" }}>{progressBar.prompt}</Text>
                      {progressBar.showButton ? (
                        <Button
                          uppercase={false}
                          mode="contained"
                          style={NEXT_BUTTON}
                          color={buttonColor}
                          labelStyle={{ color: Colors.white, fontFamily: "Gotham-Medium" }}
                          disabled={progressBar.disabled}
                          onPress={() => onNextStep(item.step)}
                        >
                          Next
                        </Button>
                      ) : null}
                    </View>
                    <View style={[PROGRESS_BAR_CONTAINER, {}]}>
                      <Progress.Bar
                        color={PROGRESS_BAR_COLOR}
                        borderWidth={0}
                        width={windowWidth * 0.8}
                        height={10}
                        style={{ backgroundColor: color.palette.white }}
                        progress={progressBar.value ? progressBar.value : 0}
                      />
                    </View>
                  </>
                ) : null}
                {progressBar.type === 1 || progressBar.type === 2 ? (
                  <>
                    <View style={CONTROL}>
                      {progressBar.messages.map((obj, index) => {
                        return (
                          <View
                            key={index}
                            style={{ flexDirection: "row", justifyContent: "space-between" }}
                          >
                            <Text>{obj.label}</Text>
                            <Text
                              style={{ fontWeight: "bold", marginLeft: 5 }}
                            >{`${obj.val} ${obj.unit}`}</Text>
                          </View>
                        )
                      })}
                    </View>
                    <View style={PROGRESS_BAR_CONTAINER}>
                      <Progress.Bar
                        color={PROGRESS_BAR_COLOR}
                        borderWidth={0}
                        width={windowWidth * 0.8}
                        height={10}
                        style={{ backgroundColor: color.palette.white }}
                        progress={progressBar.value ? progressBar.value : 0}
                      />
                    </View>
                  </>
                ) : null}
              </View>
            )
          })}
      </View>
    )
  }

  const setSliderValue = (value: number) => {
    console.log(`SliderValue: ${value}`)
    setCookSettings((item: CookSettingsProps) => ({ ...item, doneness: value }))
  }

  const setThickness = (index: number) => {
    if (index === 0) {
      setCookSettings((item: CookSettingsProps) => ({
        ...item,
        thickness: cookSettings.thickness - 1,
      }))
    } else {
      setCookSettings((item: CookSettingsProps) => ({
        ...item,
        thickness: cookSettings.thickness + 1,
      }))
    }
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <Appbar.Header>
        <Appbar.BackAction onPress={() => navigation.goBack()} />
        <Appbar.Content
          title={
            <View style={{ alignItems: "center", justifyContent: "center" }}>
              <Text
                style={{
                  width: windowWidth * 0.5,
                  fontSize: 16,
                  fontFamily: "Gotham-Bold",
                  textAlign: "center",
                }}
                numberOfLines={2}
              >
                Grilled ribeye and cauliflower steak
              </Text>
            </View>
          }
          style={{ flex: 1 }}
        />
      </Appbar.Header>
      <ScrollView ref={(ref) => (scrollViewRef.current = ref)}>
        <View
          style={{
            backgroundColor: Colors.white,
            borderBottomLeftRadius: 20,
            borderBottomRightRadius: 20,
            marginBottom: 30,
          }}
        >
          <View>
            <Image style={{ height: windowHeight / 3, width: windowWidth }} source={STEAK_IMG} />
          </View>
          <View style={{ margin: 20 }}>
            <Text style={TEXT_LABEL}>Cook time: 40 minutes</Text>
            <View style={{ margin: 20 }}>
              <Text style={TEXT_LABEL_BOLD}>Doneness</Text>
              <View
                style={{ flex: 1, alignItems: "stretch", justifyContent: "center", marginTop: 20 }}
              >
                <View
                  style={{
                    flexDirection: "row",
                    justifyContent: "space-between",
                    marginBottom: -10,
                  }}
                >
                  <Text style={TEXT_LABEL}>Rare</Text>
                  <Text style={TEXT_LABEL}>Medium</Text>
                  <Text style={TEXT_LABEL}>Well</Text>
                </View>
                <Slider
                  maximumValue={9}
                  minimumValue={1}
                  step={1}
                  value={cookSettings.doneness}
                  onValueChange={setSliderValue}
                  maximumTrackTintColor={color.palette.lightGrey50}
                  minimumTrackTintColor={color.palette.transparent}
                  trackStyle={{
                    height: 8,
                    borderRadius: 20,
                  }}
                  thumbStyle={{ height: 15, width: 15, backgroundColor: Colors.grey600 }}
                />
                <View
                  style={{ flexDirection: "row", justifyContent: "space-between", marginTop: -8 }}
                >
                  {numbers.map((item, index) => (
                    <Text style={{ fontSize: 10, color: color.palette.lightGrey100 }} key={index}>
                      {item}
                    </Text>
                  ))}
                </View>
              </View>
              <View
                style={{
                  flexDirection: "row",
                  alignItems: "center",
                  justifyContent: "space-between",
                  marginVertical: 20,
                }}
              >
                <Text style={TEXT_LABEL_BOLD}>Thickness</Text>
                <Text style={TEXT_LABEL}>{cookSettings.thickness} in</Text>
                <View
                  style={{
                    flexDirection: "row",
                    alignItems: "center",
                  }}
                >
                  <IconButton
                    icon="minus"
                    color={Colors.white}
                    style={{ backgroundColor: Colors.grey600 }}
                    size={25}
                    disabled={cookSettings.thickness === 1}
                    onPress={() => setThickness(0)}
                  />
                  <IconButton
                    icon="plus"
                    color={Colors.white}
                    style={{ backgroundColor: Colors.grey600 }}
                    size={25}
                    onPress={() => setThickness(1)}
                  />
                </View>
              </View>
              <View style={{ marginVertical: 35, alignItems: "center" }}>
                <Button
                  uppercase={false}
                  mode="contained"
                  style={{ borderRadius: 100 }}
                  labelStyle={{ color: Colors.white, fontFamily: "Gotham-Book", fontSize: 19 }}
                  contentStyle={BUTTON_CONTENT_STYLE}
                  color={buttonColor}
                  disabled={startStatus}
                  onPress={() => onNextStep(0)}
                >
                  Start Cooking!
                </Button>
              </View>
              <View style={{ marginBottom: 30 }}>
                <Text style={TEXT_LABEL_BOLD}>Ingredients:</Text>
                <>
                  {cookSettings.ingredients.map((item, index) => {
                    return <Paragraph key={index}>{item}</Paragraph>
                  })}
                </>
              </View>
            </View>
          </View>
        </View>
        <>
          {cookingSteps.map((data, index) => {
            return <StepItem item={data} index={index} key={index} />
          })}
        </>
        <View style={[{ marginTop: 30 }, enjoyStatus ? GRAYSCALE_STYLE : {}]}>
          <ImageBackground
            style={{
              height: windowHeight / 3,
              width: windowWidth,
              alignItems: "center",
              justifyContent: "center",
            }}
            source={STEAK_IMG}
          >
            <Button
              onPress={() => navigation.dispatch(StackActions.popToTop())}
              uppercase={false}
              labelStyle={{ color: Colors.white, fontFamily: "Gotham-Book", fontSize: 16 }}
              style={{ width: windowWidth / 3, borderRadius: 20 }}
              mode="contained"
              disabled={enjoyStatus}
              color={buttonColor}
            >
              Enjoy!
            </Button>
          </ImageBackground>
        </View>
      </ScrollView>
      <Modal source={require("./steak_4_1.png")} />
    </Screen>
  )
})
