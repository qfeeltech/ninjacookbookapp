/* eslint-disable react-native/no-inline-styles */
import React, { useEffect } from "react"
import { observer } from "mobx-react-lite"
import { ViewStyle, View, Image, Dimensions, Text } from "react-native"
import { Screen } from "../../components"
import { useNavigation } from "@react-navigation/native"
// import { useStores } from "../../models"
import { color } from "../../theme"
import { Appbar, Title, Caption } from "react-native-paper"

const windowWidth = Dimensions.get("window").width
const windowHeight = Dimensions.get("window").height

const ROOT: ViewStyle = {
  backgroundColor: color.palette.white,
  flex: 1,
}

export const SetupNinjaScreen = observer(function SetupNinjaScreen() {
  // Pull in one of our MST stores
  // const { someStore, anotherStore } = useStores()

  const timeoutRef = React.useRef(undefined)

  // Pull in navigation via hook
  const navigation = useNavigation()

  useEffect(() => {
    console.log("Setup Ninja Screen")
    timeoutRef.current = setInterval(() => {
      timeoutRef.current && clearInterval(timeoutRef.current)
      const { index, routes } = navigation.dangerouslyGetState()
      const currentRoute = routes[index].name
      console.log("current screen", currentRoute)
      if (currentRoute === "setup") {
        navigation.navigate("pairing")
        console.log("timeout 15s nav to pairing screen")
      }
    }, 1000 * 10)

    return () => {
      console.log("Setup Ninja Screen close.")
      timeoutRef.current && clearInterval(timeoutRef.current)
    }
  }, [])

  const _goBack = () => {
    timeoutRef.current && clearInterval(timeoutRef.current)
    navigation.goBack()
  }

  return (
    <Screen style={ROOT} preset="scroll">
      <Appbar.Header>
        <Appbar.BackAction onPress={() => _goBack()} />
        <Appbar.Content title="Setup Ninja" titleStyle={{ fontSize: 18, fontWeight: "bold" }} />
      </Appbar.Header>
      <View style={{ alignItems: "center", justifyContent: "center", marginTop: 30 }}>
        <Image
          style={{ width: windowWidth, height: windowHeight * 0.4, resizeMode: "center" }}
          source={require("./power.png")}
        />
      </View>
      <View style={{ flex: 1, alignItems: "center", margin: 35 }}>
        <Title>Product plugged in and ready</Title>
        <View style={{ marginTop: 10 }}>
          <Caption style={{ fontSize: 16, textAlign: "center" }}>
            Press the power button on your Ninja to setup.
          </Caption>
          <Caption style={{ fontSize: 16, textAlign: "center" }}>
            Make sure your phone is connected to a Wi-Fi network and you have the Wi-Fi password
            ready.
          </Caption>
        </View>
      </View>
    </Screen>
  )
})
